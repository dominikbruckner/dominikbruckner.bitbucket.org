'use strict';
var day_of_week = new Array('So','Mo','Di','Mi','Do','Fr','Sa');
var month_of_year = new Array('Januar','Februar','M�rz','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember');

//  DECLARE AND INITIALIZE VARIABLES
var now2 = new Date();
var now,selectedDay,Calendar;
var DAYS_OF_WEEK = 7;   
var DAYS_OF_MONTH = 31;  

initialize();
function initialize() {
    now = new Date();
    selectedDay = now.getDate();
    Calendar = now;
    updateCalendar();
    var zw = new Date(now.getFullYear(),now.getMonth()-1,selectedDay);
    var weekdays = ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'];
    document.getElementById('weekday').innerHTML = weekdays[zw.getDay()];
    document.getElementById('dateSmall').innerHTML = selectedDay;
    document.getElementById('calendar').style.zIndex='-1';
    document.getElementById('listener').style.zIndex='-1';
    document.getElementById('calendar').style.display='none';
    document.getElementById('listener').style.display='none';
    //localStorage.setItem('date',now.getFullYear()+'|'+now.getMonth()+'|'+selectedDay);
}
document.getElementById('listener').addEventListener('click',function(e){
    selectedDay = getPressedDate(e);
    Calendar=new Date(now.getFullYear(),now.getMonth()-1,now.getDate());
    updateCalendar();
    
    var zw = new Date(now.getFullYear(),now.getMonth()-1,selectedDay);
    var weekdays = ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'];
    document.getElementById('weekday').innerHTML = weekdays[zw.getDay()];
    document.getElementById('dateSmall').innerHTML = selectedDay;
    //localStorage.setItem('date',now.getFullYear()+'-'+now.getMonth()+'-'+selectedDay);
});

function setMonth(el) {
    var zw=0;
    if(el.id.split('|')[1]=='+'){zw=2;}else{zw=0;}
    Calendar = new Date(1900+now.getYear(),now.getMonth()-2+zw,now.getDate());
    selectedDay=null;
    updateCalendar();
}

function setYear(el) {
    var zw=0;
    if(el.id.split('|')[1]=='+'){zw=2;}else{zw=0;}
    Calendar = new Date(1900+now.getYear()-1+zw,now.getMonth()-1,now.getDate());
    selectedDay=null;
    updateCalendar();
}

function getYear() {
    return now.getYear()+1900;
}

function getMonth() {
    return now.getMonth();
}

function getDay() {
    return selectedDay;
}

function getPressedDate(e) {
    var pos = getPosition('listener');
    var zw1=selectedDay;
    var zw2 = new Date(1900+now.getYear(),now.getMonth()-1,1);
    pos.x = Math.ceil((e.x - pos.x)/53);
    pos.y = Math.ceil((e.y - pos.y)/22);
    selectedDay = (pos.y-1)*7 + pos.x-zw2.getDay();
    zw2 = new Date(1900+now.getYear(),now.getMonth(),0);
    if(selectedDay<1 ||  selectedDay>zw2.getDate()){selectedDay=zw1;}
    return selectedDay;
}

function getPosition(element) {
	element = document.getElementById(element);
    var xPosition = 0;
    var yPosition = 0;
  
    while(element) {
        xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
        yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
        element = element.offsetParent;
    }
    return { x: xPosition, y: yPosition };
}

function display() {
    if(document.getElementById('calendar').style.zIndex=='-1'){
        document.getElementById('calendar').style.zIndex='5';
        document.getElementById('listener').style.zIndex='6';
        document.getElementById('calendar').style.display='block';
        document.getElementById('listener').style.display='block';
    }else {
        document.getElementById('calendar').style.zIndex='-1';
        document.getElementById('listener').style.zIndex='-1';
        document.getElementById('calendar').style.display='none';
        document.getElementById('listener').style.display='none';
    }
}

function updateCalendar() {

    var year = Calendar.getFullYear();  
    var month = Calendar.getMonth();   
    var cal,index;    
    
    var TR_start = '<TR>';
    var TR_end = '</TR>';
    var highlight_start_selected = '<TD WIDTH="53px"><TABLE CELLSPACING=0 BORDER=1 BGCOLOR=DEDEFF BORDERCOLOR=CCCCCC><TR>';
    var highlight_end_selected  = '</TR></TABLE>';
    var highlight_start = '<TD WIDTH="50px"><B><CENTER>';
    var highlight_end   = '</CENTER></B></TD>';
    var TD_start = '<TD WIDTH="53"><CENTER>';
    var TD_end = '</CENTER></TD>';
    
    Calendar.setDate(1); 
    Calendar.setMonth(month); 

    cal =  '<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=0 BORDERCOLOR=BBBBBB style="width:375px;background-color:white;"><TR><TD>';
    cal += '<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=2>' + TR_start;
    cal += '<TD COLSPAN="' + DAYS_OF_WEEK + '" BGCOLOR="#EFEFEF"><CENTER><B>';
    cal += "<input type='button' value='<<' id='year|-' onclick='setYear(this)'/><input type='button' value='<' id='month|-' onclick='setMonth(this)'/>";
    cal += '&nbsp&nbsp&nbsp&nbsp'+month_of_year[month]  + '   ' + year + '&nbsp&nbsp&nbsp&nbsp'+'</B>';
    cal += "<input type='button' value='>' id='month|+' onclick='setMonth(this)'/><input type='button' value='>>' id='year|+' onclick='setYear(this)'/>";
    cal += TD_end + TR_end;
    cal += TR_start;

    for(index=0; index < DAYS_OF_WEEK; index++) {
        if(index == now2.getDay() && Calendar.getYear() == now2.getYear() && Calendar.getMonth() == now2.getMonth()) {
            cal += TD_start + '<B>' + day_of_week[index] + '</B>' + TD_end;
        } else {
            cal += TD_start + day_of_week[index] + TD_end;
        }
    }
    cal += TD_end + TR_end;
    cal += TR_start;

    for(index=0; index < Calendar.getDay(); index++) {
        cal += TD_start + '  ' + TD_end;
    }

    for(index=0; index < DAYS_OF_MONTH; index++) {
        if( Calendar.getDate() > index ) {
            var week_day =Calendar.getDay();
            if(week_day == 0) {
                cal += TR_start;
            }
            if(week_day != DAYS_OF_WEEK) {
                var day  = Calendar.getDate();
                if( index+1 == selectedDay ) {
                    cal += highlight_start_selected;
                }
                if( now2.getDate()==index+1  && Calendar.getYear() == now2.getYear() && Calendar.getMonth() == now2.getMonth()) {
                    cal += highlight_start + day + highlight_end + TD_end;
                }else{
                    cal += TD_start + day + TD_end;
                }
            }
            if( index+1 == selectedDay ) {
                cal += highlight_end_selected;
            }
            if(week_day == DAYS_OF_WEEK) {
                cal += TR_end;
            }
        }
        Calendar.setDate(Calendar.getDate()+1);
    }
    cal += '</TD></TR></TABLE></TABLE>';
    now = Calendar;
    document.getElementById('calendar').innerHTML = cal;
}
