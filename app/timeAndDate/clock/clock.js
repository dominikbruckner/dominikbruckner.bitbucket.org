'use strict';
//============================================================================================================================================
//============================================================================================================================================
// CLOCK MINUTE/SECOND
/*
var degrees=[0,0],looper=[0,0],time=[-1,0,0],ev={'x':0,'y':0},browser=getBrowser();
function changeTime() {
	var zw='',zw2='';
	if(time[0]<10){zw='0';}
	if(time[1]<10){zw2='0';}
	setTimeout('changeTime()',100);
}

function rotateAnimation(el,speed,index){
	var elem = document.getElementById(el);
	if(browser=='chrome'){
		elem.style.WebkitTransform = "rotate("+degrees[index]+"deg)";
	} else if(browser=='firefox'){
		elem.style.MozTransform = "rotate("+degrees[index]+"deg)";
	} else if(browser=='ie'){
		elem.style.msTransform = "rotate("+degrees[index]+"deg)";
	} else if(browser=='opera'){
		elem.style.OTransform = "rotate("+degrees[index]+"deg)";
	} else {
		elem.style.transform = "rotate("+degrees[index]+"deg)";
	}
	looper[index] = setTimeout('rotateAnimation(\''+el+'\','+speed+','+index+')',speed);
	degrees[index]+=6;
	time[index]++;
	if(degrees[index] > 359){
		degrees[index] = 0;
		time[0] = 0;
		time[2] ++;
	}
}

function initializeRealClock() {
	changeTime();
	rotateAnimation('minute2',1000,0);
	rotateAnimation('hour2',60*1000,1);
}
*/
//============================================================================================================================================
//============================================================================================================================================





//============================================================================================================================================
//============================================================================================================================================

var angleMin=0,angleH=0,dayTime=0,ev={'x':0,'y':0},browser=getBrowser(),time;
initialize();

function initialize() {
	document.getElementById('hour').ondragstart = function() { return false; };
	document.getElementById('minute').ondragstart = function() { return false; };
	document.getElementById('background_clock').ondragstart = function() { return false; };
	//localStorage.setItem('time','00:00');
}

function timeClicked(div) {
	var id=div.id;
	if(id==='before') {id='after';dayTime=0;} else {id='before';dayTime=12;}
	div.style.borderColor='red';
	document.getElementById(id).style.borderColor='black';
}

function setMinute() {
	var el=document.getElementById('background_clock');
	var zw = getPosition('background_clock');
	var x=zw.x+el.width/2-ev.x;
	var y=zw.y+el.height/2-ev.y;
	angleMin = Math.atan(-x/y)*360/(2*Math.PI);
	if(y<0){angleMin+=180;}
	var angleMinZw = angleMin/360;
	if(angleMin<0){angleMinZw++;}
	rotate('minute',Math.round(angleMinZw*60)*6);
	var angleHZw = angleH/360;
	if(angleH<0){angleHZw++;}
	if(Math.round(angleMinZw*60)*6 != 0) {
		rotate('hour',Math.round(angleMinZw*60)*0.5+Math.round(12*angleHZw)*30);
	} else {
		rotate('hour',Math.round(12*angleHZw)*30);
	}
}

function setHour() {
	var el=document.getElementById('background_clock');
	var zw = getPosition('background_clock');
	var x=zw.x+el.width/2-ev.x;
	var y=zw.y+el.height/2-ev.y;
	angleH = Math.atan(-x/y)*360/(2*Math.PI);
	if(y<0){angleH+=180;}
	var angleHZw = angleH/360;
	if(angleH<0){angleHZw++;}
	var angleMinZw = angleMin/360;
	if(angleMin<0){angleMinZw++;}
	rotate('hour',Math.round(12*angleHZw)*30+Math.round(angleMinZw*60)*0.5);
}

function getPosition(element) {
	element = document.getElementById(element);
    var xPosition = 0;
    var yPosition = 0;
  
    while(element) {
        xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
        yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
        element = element.offsetParent;
    }
    return { x: xPosition, y: yPosition };
}

function rotate(el,degree){
	var elem = document.getElementById(el);
	if(browser=='chrome'){
		elem.style.WebkitTransform = "rotate("+degree+"deg)";
	} else if(browser=='firefox'){
		elem.style.MozTransform = "rotate("+degree+"deg)";
	} else if(browser=='ie'){
		elem.style.msTransform = "rotate("+degree+"deg)";
	} else if(browser=='opera'){
		elem.style.OTransform = "rotate("+degree+"deg)";
	} else {
		elem.style.transform = "rotate("+degree+"deg)";
	}
}

function getTime() {
	var min,hour;
	var angleMinZw = angleMin/360;
	if(angleMin<0){angleMinZw++;}
	min=Math.round(angleMinZw*60);
	if(min==60){min=0;}
	var angleHZw = angleH/360;
	if(angleH<0){angleHZw++;}
	hour=Math.round(angleHZw*12+dayTime);
	return {'hour':hour , 'min':min};
}

function getBrowser() {
	var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
	var isFirefox = typeof InstallTrigger !== 'undefined';  
	var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
	var isChrome = !!window.chrome && !isOpera;             
	var isIE = false || !!document.documentMode;
	if(isOpera){return 'opera';}
	if(isFirefox){return 'firefox';}
	if(isSafari){return 'safari';}
	if(isChrome){return 'chrome';}
	if(isIE){return 'ie';}
}

//============================================================================================================================================
//============================================================================================================================================




//============================================================================================================================================
//============================================================================================================================================

document.getElementById('clock').addEventListener('mousemove',function(e){
	ev = e;
	time = getTime();
	//localStorage.setItem('time',time.hour+'|'+time.min);
});

var mousedownID = -1; 
var id = document.getElementById('minute');
function mousedown(event) {
	if(mousedownID==-1) {
		mousedownID = setInterval(setMinute, 100);
	} 
}
function mouseup(event) {
	if(mousedownID!=-1) {  
		clearInterval(mousedownID);
		mousedownID=-1;	
	}
}

id.addEventListener("mousedown", mousedown);
document.addEventListener("mouseup", mouseup);


var mousedownID2 = -1; 
var id2 = document.getElementById('hour');
function mousedown2(event) {
	if(mousedownID2==-1) {
		mousedownID2 = setInterval(setHour, 100);
	} 
}
function mouseup2(event) {
	if(mousedownID2!=-1) {  
		clearInterval(mousedownID2);
		mousedownID2=-1;
	}
}

id2.addEventListener("mousedown", mousedown2);
document.addEventListener("mouseup", mouseup2);

//============================================================================================================================================
//============================================================================================================================================





