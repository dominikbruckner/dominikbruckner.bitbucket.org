'use strict';
var messages=[],
    counter=-1,
    backForward=[],
    i,
    len,
    forward='forward',
    backward='backward',
    isCounterEnd=false;
    
/* ======================================================================================================================================== */
/* ========================================== MESSAGES IN BUBBLE AT TUTORIAL PAGE ========================================================= */
/* ================================ additional ones can be added without changing something =============================================== */

messages[0]='Hallo und herzlich willkommen zum Routenplaner speziell f�r Sch�ler.';
messages[1]='Ich werde dir helfen dich auf dieser Webseite zurecht zu finden.';
messages[2]='Bevor du auf "Zum Routenplaner" klickst, schaue dir das kurze Einf�hrungs-Video an.';
messages[3]='Hier kommt sp�ter das Video rein';
messages[4]='Jetzt bist du bereit um zu starten. Klicke auf "Zum Routenplaner" und wir k�nnen loslegen!';

/* ======================================================================================================================================== */


initialize();

function initialize() {
    len = messages.length-1;
    backForward[0]='<input type="image" onclick="message(forward)" src="../images/forArrow.png" id="forward" style="width:10%; height:auto;"/>';
    backForward[len]='<input type="image" onclick="message(backward)" src="../images/backArrow.png" id="backward" style="width:10%; height:auto;"/>';
    for(i=1;i<len;i++){
        backForward[i] = backForward[len] + backForward[0];
    }
    document.getElementById('bubble2').className='bubble2';
    document.getElementById('message2').className='message2';
    document.getElementById('toRouting').innerHTML = 'Noch '+(len)+' Klicks bis zum Routenplaner!';
    message();
}

function message(id) {
    if(typeof id == 'undefined'){id='forward'};
    if(id=='forward') {counter++;} else {counter--;}
    say2('<p>' + messages[counter] + '</p>' + backForward[counter]);
    document.getElementById('toRouting').innerHTML = 'Noch '+(len-counter)+' Klicks bis zum Routenplaner!';
    if(counter==len){
        isCounterEnd = true;
        document.getElementById('toRouting').className = "action-button shadow animate green";
        document.getElementById('toRouting').innerHTML = 'Zum Routenplaner!';
    } else {
        isCounterEnd = false;
        document.getElementById('toRouting').className = "action-button shadow animate gray";
        document.getElementById('toRouting').innerHTML = 'Noch '+(len-counter)+' Klicks bis zum Routenplaner!';
    }
}

function direction() {
    if(isCounterEnd) {
        window.location = './main.html';
    }
}
















