var zoomControl = function (map) {
    this.map = map;
    var color = 'green';
    this.getColor = getColorControl(color);
    this.getZoom = getZoom(map);
    this.zoomIn = zoomInControl;
    this.zoomOut = zoomOutControl;
}

function getZoom(map) {
    this.map = map;
    return map.getZoom();
}

function zoomInControl(map) {
    map.zoomIn();
}

function zoomOutControl(map) {
    map.zoomOut();
}
    
function getColorControl(color) {
    return color;
}
