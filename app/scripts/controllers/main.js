'use strict';
/* ================================================================================================================================ */
var wantToTestDistance=false;         // in exportData and getGeoJson !

var ROUTERECALCULATION = 2;           //route_length / air_length
var TIMER = 5*1000 ;                  //set timeout for *ms
var DIST = 1;                         // length of separated route segments (in meters)


var sound=false;
var i,audio=[];
for(i=1;i<12;i++){audio[i] = new Audio('../audio/'+i+'.mp3');}

var myID = 'fritzeflink.ipkbgifo';
//var myID = 'fritzeflink.58f0eaaa';
var accessToken ='pk.eyJ1IjoiZnJpdHplZmxpbmsiLCJhIjoiR1ZHMTQ5VSJ9.pvciELn0vNNaNiKaA6Cr0g';
//var myID = 'brucknerdominik.m5c579e8';
//var accessToken = 'pk.eyJ1IjoiYnJ1Y2tuZXJkb21pbmlrIiwiYSI6InNwY2IwbDQifQ.JavChGjj-uuFJvXuMr28Zg';
var apiKey = '0572789c-f15c-4700-b999-e318402d6530';

// leaflet                 <-- no pedestrian-routing , inf routings per day
// graphhopper             <--    pedestrian&bike-routing available , only 500 routings per day 
// mapzen                  <--    --------------"------------------ , limited by unknown amount : https://github.com/mapzen/routing/wiki/OSRM-Service-Details-and-Terms-of-Use , works most time 

var routers = 'mapzen';
/* ================================================================================================================================ */


// GLOBAL VARBIABLES
var markers=[],
	coord=[],
	route=[],
	choiceOfVeh=[],
	lines=[],
	vehicle='foot',
	dangerPoints=[],
	dangerMessage=[],
	storedDevice=[],
	dangerMarkers=[],
	timerID=-1,
	zoom,
	
	// test variables
	isStartPressed=false,
	isEndPressed=false,
	isClockPressed=false,
	isFirstClicked=false,
	isEndClicked=false,
	isInputsInserted=false,
	isRouteRunning = false,
	isRouteInitialized = false,
	isInitializeFinished = false,
	
	// inputs (need to be global)
	startTime='',
	startDate='',
	amount=0,
	id=null;
	
	

/* ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
// LOADING
var loader = document.getElementById('loader');
startLoading();
function startLoading() {
	loader.className='';
	console.log('LOADING IN PROGRESS');
}
function finishedLoading() {
	var timerID = setInterval(function(){
		if(isRouteInitialized && isInitializeFinished){
			loader.classname='hide';
			loader.className='done';
			console.log('LOADING COMPLETE');
			clearInterval(timerID);
		}
	},100);
}
/* ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
// MAP
var map = L.map('map', myID, {
    zoomControl: false,
	scrollWheelZoom: true,
	attributionControl: false,
}).setView([48.15, 11.59], 13);
map.zoomControl.removeFrom(map);
L.tileLayer('http://{s}.tiles.mapbox.com/v3/{mapId}/{z}/{x}/{y}.png?access_tolen={accessToken}', {
    minZoom: 12,
    maxZoom: 18,
	mapId: myID,
	accessToken: accessToken
}).addTo(map).on('load',finishedLoading);
function myZoom(id) {if(id=='+'){map.zoomIn();}else{map.zoomOut();}}

/* ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */


initialize();


// FUNCTIONS
// INITIALIZATION
/**
If markers are available at the local storage of browser then load them with additional design and properties & calculate route afterwards
**/

function initialize() {
	document.getElementById('bubble').style.display='none';
	document.getElementById('message').style.display='none';
	zoom=map.getZoom();
	if(sound){audio = audio[1].play();}
	addEventListener('mousedown',function(e){openPopupByRightMouse(e);});
	mapZoomEnabled(false);
	dangerPoints=[];
	dangerMessage=[];
	dangerMarkers=[];
	lines=[];
	route=[];
	choiceOfVeh=[];
	markers=[];
	storedDevice=[];
	coord=[];
	vehicle = 'foot';
	hideInputs();
	document.getElementById('rectangle').style.top =  0-4 + '%';
	if(getStoredValue('time')!=null) {
		startTime = getStoredValue('time')[0];
		var zw1='',zw2='',zw3=startTime.split(':');
		if(zw3[0]<10){zw1=0;}
		if(zw3[1]<10){zw2=0;}
		document.getElementById('time').value=zw1+zw3[0]+':'+zw2+zw3[1];
	}
	if(getStoredValue('date')!=null) {
		startDate = getStoredValue('date')[0];
		var zw1='',zw2='',zw3=startDate.split('-');
		if(zw3[1]<10){zw1=0;}
		if(zw3[2]<10){zw2=0;}
		document.getElementById('date').value=zw3[0]+'-'+zw1+zw3[1]+'-'+zw2+zw3[2];
	}
	if(getStoredValue('amount')!=null) {
		amount = getStoredValue('amount')[0];
	}
	if(getStoredValue('markers')!=null) {
		var storedMarkers = getStoredValue('markers');
		var test = storedMarkers[0].split("(");
		var i;
		if (test[0]=='LatLng') {
			if(getStoredValue('device')!=null) {
				storedDevice = getStoredValue('device')[0].split(',');
				var value;
				switch (storedDevice[storedDevice.length-1]) {
					case 'foot':
						value = 0;
						break;
					case 'bike':
						value = 1;
						break;
					case 'bicycle':
						value = 1;
						break;
					case 'car':
						value = 2;
						break;
					case 'bus':
						value = 3;
						break;
					case 'underground':
						value = 4;
						break;
					default: console.log('Wrong or no device identifier at local storage!');
				}
				document.getElementById('rectangle').style.top = value * 20 - 4 + '%';
			}
			getStoredMarkers(storedMarkers,'markers');
			var ml = markers.length;
			switch(ml) {
				case 1:
					isStartPressed = true;
					isClockPressed = true;
					document.getElementById('start').value = getStoredValue('address1');
					break;
				case 2:
					isStartPressed = true;
					isEndPressed = true;
					isClockPressed = true;
					isInputsInserted = true;
					document.getElementById('start').value = getStoredValue('address1');
					document.getElementById('end').value = getStoredValue('address2');
					document.getElementById('transportation').style.zIndex="3";
					mapZoomEnabled(true);
					break;
				default:
					isStartPressed = true;
					isEndPressed = true;
					isFirstClicked = true;
					isClockPressed = true;
					markers[0].closePopup();
					document.getElementById('start').value = getStoredValue('address1');
					document.getElementById('end').value = getStoredValue('address2');
					document.getElementById('transportation').style.zIndex="3";
					mapZoomEnabled(true);
			}
		}
	} else {isRouteInitialized=true;}
	/*if(getStoredValue('dangerMarkers')!=null && storedMarkers[0].split("(")[0]=='LatLng') {
		var i,storedMarkers = getStoredValue('dangerMarkers')[0].split('|');
		getStoredMarkers(storedMarkers,'dangerMarkers');
	}*/
	if(isClockPressed && isStartPressed && isEndPressed && amount!=0) {
		showAllInputs();
		map.setView(markers[0]._latlng,18);
	} else {
		document.getElementById('bubble2').className='bubble2';
		document.getElementById('message2').className='message2';
		say2('<p>Hallo, ich werde dir helfen, deinen Weg zu planen. Gib deine Startaddresse ein und klicke dann auf den gr�nen Haken! <u>Tipp:</u>'+
			 ' Addressen wie z.B. "TU M�nchen" funktionieren auch!</p>' +
			 '<input type="image" src="../images/OK.png" onclick="firstInputText(0)" style="position:relative;width:6.89%;height:26.67%;">');
	}
	if(getStoredValue('isEndClicked')!=null) {
		isEndClicked = getStoredValue('isEndClicked')[0];
		hideInputs();
		if(isEndClicked) {
			document.getElementById('dangerMod').style.zIndex='3';
			routingEnded();
		}
		/*if(getStoredValue('dangerMessage')!=null) {
			var zw = getStoredValue('dangerMessage');
			for(i=0;i<zw.length;i++) {
				getDangerMessage(zw[i]+i);
			}
		}*/
	}
	if(markers.length==2) {
		showInput('bubble');
		showInput('message');
		say('Klicke auf ein Transportmittel und dann auf den Startpunkt!');
		highlightText('message');
	}
	if(markers.length>2) {
		showInput('bubble');
		showInput('message');
		say('Klicke auf ein Transportmittel und dann auf den n�chsten Wegpunkt! <u>Tipp:</u> Wenn du mit dem Auto, U/S-Bahn oder Bus f�hrst, '+
			'reicht es den Anfangs- und Endpunkt anzuklicken!');
		highlightText('message');
	}
	document.getElementById('removeAllMarkers').ondragstart = function() { return false; };
	document.getElementById('helper').ondragstart = function() { return false; };
	document.getElementById('transportation').ondragstart = function() { return false; };
	document.getElementById('backForward').ondragstart = function() { return false; };
	setMarkersSize();
	if(markers.length>2){markers[0].closePopup();}
	if(markers.length<2){setToUserPosition();}
	isInitializeFinished = true;
}

function hideInputs() {
	document.getElementById('date').style.display='none';
	document.getElementById('time').style.display='none';
	document.getElementById('start').style.display='none';
	document.getElementById('end').style.display='none';
	document.getElementById('startOK').style.display='none';
	document.getElementById('endOK').style.display='none';
	document.getElementById('endOfRouting').style.display='none';
	document.getElementById('removeAllMarkersButton').style.display='none';
	
	document.getElementById('fstart').style.display='none';
	document.getElementById('fstartOK').style.display='none';
	document.getElementById('fend').style.display='none';
	document.getElementById('fendOK').style.display='none';
	document.getElementById('dateInput').style.display='none';
	document.getElementById('timeInput').style.display='none';
	document.getElementById('fclockOK').style.display='none';
	document.getElementById('famount').style.display='none';
	document.getElementById('famountOK').style.display='none';
	document.getElementById('firstInput').style.zIndex="-1";
	document.getElementById('transportation').style.zIndex="-1";
	document.getElementById('endOfRoutingAndDanger').style.display = 'none';
	document.getElementById('backToRouting').style.zIndex = '-1';
	
	document.getElementById('zoom').style.display = 'none';
	document.getElementById('printer').style.display = 'none';
}

function showInput(key) {
	document.getElementById(key).style.display='block';
}

function showAllInputs() {
	document.getElementById('date').style.display='block';
	document.getElementById('time').style.display='block';
	document.getElementById('start').style.display='block';
	document.getElementById('end').style.display='block';
	document.getElementById('startOK').style.display='block';
	document.getElementById('endOK').style.display='block';
	document.getElementById('endOfRouting').style.display='block';
	document.getElementById('removeAllMarkersButton').style.display='block';
	document.getElementById('date').value=startDate;
	document.getElementById('time').value=startTime;
	document.getElementById('start').value = getStoredValue('address1');
	document.getElementById('end').value = getStoredValue('address2');
	showInput('zoom');
	showInput('printer');
}

function mapZoomEnabled(enable) {
	if(enable) {
		map.dragging.enable();
		map.touchZoom.enable();
		map.doubleClickZoom.enable();
		map.scrollWheelZoom.enable();
	} else {
		map.dragging.disable();
		map.touchZoom.disable();
		map.doubleClickZoom.disable();
		map.scrollWheelZoom.disable();
	}
}


// HANDLE MOUSE CLICKS ON MAP 
/**
handle clicks on map (together with the "DRAW AND DELETE MARKERS"-section some sort of main program)
**/

map.on('click',function(e) {
	if(!isEndClicked) {
		if(isStartPressed && isEndPressed && isClockPressed && isFirstClicked && !isRouteRunning) {
			addMarker(e);
			if(markers.length>3) {
				addRoute(markers.length-4,0);
			}
			changeIcon(markers.length-2);
		} else {
			if(markers.length>1){markers[0].openPopup();}
		}
	} else {
	}
});

map.on('zoomend',function(){
	setMarkersSize();
});


// DRAW AND DELETE MARKERS
/** 
addMarker: add Marker to markers-array and add to map
onPopupOpen: delete marker by clicking button in popup
deleteMarker: remove marker in markers-array
updatePosition: update of position of dragged marker -> new position in markers-array and route is recalculated  
**/

function addMarker(e) {
	var newMarker = L.marker([e.latlng.lat , e.latlng.lng],{'draggable': true});
	if (markers.length == 2) {
		newMarker.options.draggable = false; 
		say('Klicke auf die Karte um den n�chsten Punkt zu setzen. Achtung: Setzte deine Punkte nicht zu weit auseinander!');
		highlightText('message');
		if(sound){audio[6].stop();audio[7].play();}
	}
	if(markers.length >2) {
		say('Klasse. Mache so weiter, bis du am Ziel bist. <u>Tipp:</u> Wenn du nicht mehr weiter wei�t, schaue dir das Video noch einmal an!');
	}
	var zwVeh = vehicle;
	if(vehicle=='bicycle'){zwVeh='bike';}
	if(vehicle=='bus'){zwVeh='H';}
	newMarker.options.icon  = L.icon({iconUrl:'../images/'+zwVeh+'.png' , iconSize :[40,40]});
	if(markers.length==3) {
		map.removeLayer(markers[markers.length-1]);
		markers[markers.length-1].options.icon = L.icon({iconUrl:'../images/'+zwVeh+'.png' , iconSize :[40,40]});
		markers[markers.length-1].addTo(map);
	}
	var zw = markers.length-1;
	newMarker.bindPopup('<input type="button" value="Wegpunkt bis hierher l�schen" class="marker-delete-button"/>');
	newMarker.on('popupopen', onPopupOpen);
	newMarker.on('dragend',updatePosition);
	markers.push(newMarker);
	markers[markers.length-1].addTo(map);
	setMarkersSize();
	return markers;
}

function changeIcon(index,initialize) {
	if(typeof initialize == 'undefined'){initialize=false;}
	var zwVeh;
	if(vehicle=='bicycle'){zwVeh='bike';}else{zwVeh=vehicle;}
	if(index>2 && initialize){
		map.removeLayer(markers[index]);
		var zwVeh2 = choiceOfVeh[index-3];
		if(zwVeh2=='bicycle'){zwVeh2='bike';}
		markers[index].options.icon = L.icon({iconUrl:'../images/'+zwVeh2+'.png' , iconSize :[80,80]});
		markers[index].addTo(map);
		setMarkersSize();
	}
	if(index==2){
		map.removeLayer(markers[index-1]);
		var zwVeh2 = choiceOfVeh[index-2];
		if(zwVeh2=='bicycle'){zwVeh2='bike';}
		markers[index-1].options.icon = L.icon({iconUrl:'../images/'+zwVeh2+'.png' , iconSize :[80,80]});
		markers[index-1].addTo(map);
		setMarkersSize();
	}
	if(index>2 && choiceOfVeh[index-3] != vehicle) {
		map.removeLayer(markers[index]);
		var zwVeh2 = choiceOfVeh[index-3];
		if(zwVeh2=='bicycle'){zwVeh2='bike';}
		markers[index].options.icon = L.icon({iconUrl:'../images/'+zwVeh2+'To'+zwVeh+'.png' , iconSize :[80,80]});
		markers[index].addTo(map);
		setMarkersSize();
	} 
}

function onPopupOpen() {
    var tempMarker = this;
    $('.marker-delete-button:visible').click(function () {
		var index = markers.indexOf(tempMarker);
		if(!isEndClicked) {
			if(index>3) {
				var zwVeh = choiceOfVeh[index-4];
				if(zwVeh=='bicycle'){zwVeh='bike';}
				if(zwVeh=='bus'){zwVeh='H';}
				map.removeLayer(markers[index-1]);
				markers[index-1].options.icon = L.icon({iconUrl:'../images/'+zwVeh+'.png' , iconSize :[40,40]});
				markers[index-1].addTo(map);
			}
			deleteMarker(index);
			deleteRoute(index);
		} else {
			map.removeLayer(tempMarker);
			var index2 = dangerPoints.indexOf(tempMarker);
			dangerPoints.splice(index2,1);
			dangerMessage.splice(index2,1);
		}
    });
}

function deleteMarker(index) {
	var i;
	for(i=index;i<markers.length;i++) {
		map.removeLayer(markers[i]);
	}
	markers = markers.splice(0,index);
	if(index==2){
		markers[0].openPopup();
		isFirstClicked=false;
		markers[0].options.icon.options.iconUrl = '../images/circle_blue_start.png';
		setMarkersSize();	
	}
}

function updatePosition() {
	var tempMarker = this;
	var index = markers.indexOf(tempMarker);
	var m = tempMarker.getLatLng();
	markers[index]._latlng.lat=m.lat;
	markers[index]._latlng.lng=m.lng;
	if(markers.length>3) {
		updateRoute(index);
	}
}

function setMarkersSize() {
	var i,size;
	for(i=0;i<markers.length;i++) {
		var zoom=map.getZoom();
		map.removeLayer(markers[i]);
		if(i>2 && i<markers.length-1 && (choiceOfVeh[i-2]!=choiceOfVeh[i-3])) {
			if(zoom==18){size=[80,80];}
			if(zoom==17){size=[60,60];}
			if(zoom==16){size=[40,40];}
			if(zoom==15){size=[30,30];}
			if(zoom==14){size=[20,20];}
			if(zoom<=13){size=[0,0];}
		} else {
			if(zoom==18){size=[40,40];}
			if(zoom==17){size=[30,30];}
			if(zoom==16){size=[20,20];}
			if(zoom==15){size=[15,15];}
			if(zoom==14){size=[10,10];}
			if(zoom<=13){size=[0,0];}
			if(i==0 || i==1) {size=[2*size[0],2*size[1]];}
		}
		markers[i].options.icon.options.iconSize=size;
		markers[i].addTo(map);
		if(i==0 && markers.length<4){
			if(zoom==18){markers[0]._popup.options.offset.y = -50;}
			if(zoom==17){markers[0]._popup.options.offset.y = -40;}
			if(zoom==16){markers[0]._popup.options.offset.y = -30;}
			if(zoom==15){markers[0]._popup.options.offset.y = -20;}
			if(zoom==14){markers[0]._popup.options.offset.y = -10;}
			if(zoom>13){markers[0].openPopup();}
		}
	}
}


// ROUTING
/**
addRoute: calculate a route between two markers
updateRoute: delete old route and calculates new one on dragged marker basis
deleteRoute: the route from the clicked marker to the end is erased
getCompleteRoute: get the complete Route on markers-array basis (only used at initialization)
getTotalDistance: get distance of route
getTime: get needed time for route (in dependency of speed)
**/

function addRoute(mk,isFirst,problem) {	
	isRouteRunning = true;
	console.log('==============================================');
	console.log('Router busy: '+isRouteRunning);
	if(typeof problem == 'undefined'){problem=false;}
	var myRouter,test=false;
	if(vehicle == 'foot' || vehicle == 'bike' || vehicle == 'bicycle') {
	/* ======================================================================================================= */
		var counter = 0;
		var timerID = setInterval(function(){counter++;},10);
	/* ======================================================================================================= */	
		switch (routers) {
			case 'graphhopper':       //graphhopper
				myRouter = L.Routing.graphHopper(apiKey,{
					urlParameters:{
						vehicle: vehicle                           
					}
				});
				break;
			case 'mapzen':       // mapzen
				myRouter = L.Routing.osrm({
					serviceUrl: 'http://osrm.mapzen.com/' + vehicle + '/viaroute'
				});
				break;
			default:
				console.log('wrong router index');
		}
		var lineDesign =  [
			{color: 'black', opacity: 0.15, weight: 8},
			{color: 'red', opacity: 0.8, weight: 0},
			{color: 'black', opacity: 0.7, weight: 8}
		];
		if(vehicle!='foot'){lineDesign[0].color='red';lineDesign[2].color='red';}
		route[mk] = L.Routing.control({
			waypoints:[
				markers[mk+2]._latlng,
				markers[mk+3]._latlng
			], 
			routeLine: function(route) {
				var line = L.Routing.line(route, {
					addWaypoints: false,
					routeWhileDragging: false,
					autoRoute: true,
					useZoomParameter: false,
					draggableWaypoints: false,
					styles: lineDesign
				});
				line.eachLayer(function(l) {
					l.on('click', function(e) {
						if(isEndClicked){addDangerMarker(e);}
					});
				});
				return line;
			},
			router: myRouter,
			fitSelectedRoutes:false,
			show:false
		});
		route[mk].addTo(map);
		
		var timerID2 = setTimeout(function(){
			if(!test && amount!=0 && isFirst==0) {
				setRouteToAirline(mk,vehicle);
			} else {
				var i,dist=0;
				if(typeof coord[mk] == 'undefined'){
					problem = true;
					isFirst = 1;
					/* ======================================================================================================= */
					console.log('The route is 0% longer than bee-line!');
					/* ======================================================================================================= */
				}
				if((problem || (!test && isFirst==1)) && 
					((vehicle=='foot' || vehicle=='bicycle' || vehicle=='bike') || (choiceOfVeh[mk]=='foot' || choiceOfVeh[mk]=='bicycle' || choiceOfVeh[mk]=='bike'))){
					if(typeof route[mk].removeFrom != 'undefined'){route[mk].removeFrom(map);}
					else{map.removeLayer(route[mk]);}
					var polyline = L.polyline(
						[markers[mk+2]._latlng , markers[mk+3]._latlng],
						{
							color: 'black',
							weight: 8,
							opacity: 0.7,
						}
					);
					if(vehicle != 'foot' && choiceOfVeh[mk] != 'foot') {polyline.options.color='red';}
					route[mk] = polyline;
					route[mk].addTo(map);
					coord[mk] = new Object();
					coord[mk].coordinates = [];
					coord[mk].coordinates[0] = [markers[mk+2]._latlng.lat,markers[mk+2]._latlng.lng];
					coord[mk].coordinates[1] = [markers[mk+3]._latlng.lat,markers[mk+3]._latlng.lng];
				}
			}
			isRouteRunning = false;
			console.log('Router busy: '+isRouteRunning);
			console.log('==============================================');
		},TIMER);
		
		route[mk].on('routeselected', function(e) {
			coord[mk] = e.route;
			test = true;
			problem = false;
			var i,dist=0;
			for(i=0;i<coord[mk].coordinates.length-1;i++) {
				var zw1 = coord[mk].coordinates[i];
				var zw2 = coord[mk].coordinates[i+1];
				dist += getDistance(zw1[0],zw1[1],zw2[0],zw2[1]);
			}
			var zw1 = markers[mk+2]._latlng;
			var zw2 = markers[mk+3]._latlng;
			var zw3 = getDistance(zw1.lat,zw1.lng,zw2.lat,zw2.lng);
			clearTimeout(timerID2);
			if(dist/zw3 >= ROUTERECALCULATION) {
				problem = true;
				isFirst = 1;
				if((problem || (!test && isFirst==1)) && 
					((vehicle=='foot' || vehicle=='bicycle' || vehicle=='bike') || (choiceOfVeh[mk]=='foot' || choiceOfVeh[mk]=='bicycle' || choiceOfVeh[mk]=='bike'))){
					if(typeof route[mk].removeFrom != 'undefined'){route[mk].removeFrom(map);}
					else{map.removeLayer(route[mk]);}
					var polyline = L.polyline(
						[markers[mk+2]._latlng , markers[mk+3]._latlng],
						{
							color: 'black',
							weight: 8,
							opacity: 0.7,
						}
					);
					if(vehicle != 'foot' && choiceOfVeh[mk] != 'foot') {polyline.options.color='red';}
					route[mk] = polyline;
					route[mk].addTo(map);
					coord[mk] = new Object();
					coord[mk].coordinates = [];
					coord[mk].coordinates[0] = [markers[mk+2]._latlng.lat,markers[mk+2]._latlng.lng];
					coord[mk].coordinates[1] = [markers[mk+3]._latlng.lat,markers[mk+3]._latlng.lng];
				}
				/*if((problem || (!test && isFirst==1)) && loader.className=='done' &&
					((vehicle=='foot' || vehicle=='bicycle' || vehicle=='bike') || (choiceOfVeh[mk]=='foot' || choiceOfVeh[mk]=='bicycle' || choiceOfVeh[mk]=='bike'))){
						setRouteToAirline(mk,vehicle);
				}*/
			}
			/* ======================================================================================================= */
			clearInterval(timerID);
			console.log('Route calculation was finished after '+counter*10+'ms!');
			/* ======================================================================================================= */
			isRouteRunning = false;
			console.log('Router busy: '+isRouteRunning);
			console.log('==============================================');
		});
	}
	
		if(vehicle == 'car' || vehicle == 'bus' || vehicle == 'underground') {
			var polyline = L.polyline(
				[markers[mk+2]._latlng , markers[mk+3]._latlng],
				{
					color: 'green',
					weight: 8,
					opacity: 0.7,
					//dashArray: '20,15',
					//lineJoin: 'round'
				}
			);
			if(vehicle == 'bus') {polyline.options.color='yellow';}
			if(vehicle == 'underground') {polyline.options.color='blue';}
			lines[mk] = polyline;
			lines[mk].addTo(map);
			route[mk]='undefined';
			isRouteRunning = false;
			/* ======================================================================================================= */
			console.log('The route is 0% longer than bee-line!');
			/* ======================================================================================================= */
			console.log('Router busy: '+isRouteRunning);
			console.log('==============================================');
		} 
	
	choiceOfVeh[mk] = vehicle;
	setMarkersSize();
}

function setRouteToAirline(index,vehicle) {
	var zw = markers[markers.length-1]._latlng;
	var e1 = new Object(),e2 = new Object();
	e1.latlng = new Object();
	e1.latlng.lat = zw.lat;
	e1.latlng.lng = zw.lng;
	e2.latlng = new Object();
	e2.latlng.lat = markers[markers.length-2]._latlng.lat + 0.5*(zw.lat - markers[markers.length-2]._latlng.lat);
	e2.latlng.lng = markers[markers.length-2]._latlng.lng + 0.5*(zw.lng - markers[markers.length-2]._latlng.lng);
	deleteMarker(index+3);
	if(route[index]!='undefined'){
		if(typeof route[index].removeFrom!='undefined'){route[index].removeFrom(map);}
		else{map.removeLayer(route[index]);}
	}
	addMarker(e2);
	addMarker(e1);
	var i;
	for(i=0;i<2;i++){
	var polyline = L.polyline(
		[markers[index+2+i]._latlng , markers[index+3+i]._latlng],
		{
			color: 'black',
			weight: 8,
			opacity: 0.7,
		}
	);
	if(vehicle != 'foot') {polyline.options.color='red';}
	route[index+i] = polyline;
	route[index+i].addTo(map);
	coord[index+i] = new Object();
	coord[index+i].coordinates = [];
	coord[index+i].coordinates[0] = [markers[markers.length-2]._latlng.lat,markers[markers.length-2]._latlng.lng];
	coord[index+i].coordinates[1] = [e2.latlng.lat,e2.latlng.lng];
	choiceOfVeh[index+i] = vehicle;
	}
	//addRoute(index+1,1);
	
	setMarkersSize();
	console.log('setRouteToAirline ended');
}

function updateRoute(index) {
	var problem1 = false,problem2 = false;
	if(index>2 && index<markers.length-1) {
		if(route[index-3]!='undefined'){
			if(typeof route[index-3].removeFrom!='undefined'){route[index-3].removeFrom(map);}
			else{map.removeLayer(route[index-3]);problem1=true;}
		}
		if(route[index-2]!='undefined'){
			if(typeof route[index-2].removeFrom!='undefined'){route[index-2].removeFrom(map);}
			else{map.removeLayer(route[index-2]);problem2=true;}
		}
		if(typeof lines[index-3]!='undefined') {map.removeLayer(lines[index-3]);}
		if(typeof lines[index-2]!='undefined') {map.removeLayer(lines[index-2]);}
		vehicle = choiceOfVeh[index-3];
		addRoute(index-3,1,problem1);
		var timerID=setInterval(function(){
			if(!isRouteRunning){
				vehicle = choiceOfVeh[index-2];
				addRoute(index-2,1,problem2);
				clearInterval(timerID);
			}
		},100);
	}
	problem1 = false;problem2 = false;
	if(index==2) {
		if(route[index-2]!='undefined'){
			if(typeof route[index-2].removeFrom!='undefined'){route[index-2].removeFrom(map);}
			else{map.removeLayer(route[index-2]);problem1=true;}
		}
		if(typeof lines[index-2]!='undefined') {map.removeLayer(lines[index-2]);}
		vehicle = choiceOfVeh[index-2];
		addRoute(index-2,1,problem1);
	}
	problem1 = false;problem2 = false;
	if(index==markers.length-1) {
		if(route[index-3]!='undefined'){
			if(typeof route[index-3].removeFrom!='undefined'){route[index-3].removeFrom(map);}
			else{map.removeLayer(route[index-3]);problem1=true;}
		}
		if(typeof lines[index-3]!='undefined') {map.removeLayer(lines[index-3]);}
		vehicle = choiceOfVeh[index-3];
		addRoute(index-3,1,problem1);
	}
}

function deleteRoute(index) {
	var i;
	if(index>3) {
		for (i=index-3;i<route.length;i++) {
			if(route[i]!='undefined'){
				if(typeof route[i].removeFrom!='undefined'){route[i].removeFrom(map);}
				else{map.removeLayer(route[i]);}
			}
			if(typeof lines[i]!='undefined'){map.removeLayer(lines[i]);}
		}
		route = route.slice(0,index-3);
		coord = coord.slice(0,index-3);
		lines = lines.slice(0,index-3);
		choiceOfVeh = choiceOfVeh.slice(0,index-3);
	} else {
		for(i=0;i<route.length;i++) {
			if(route[i]!='undefined'){
				if(typeof route[i].removeFrom!='undefined'){route[i].removeFrom(map);}
				else{map.removeLayer(route[i]);}
			}
			if(typeof lines[i]!='undefined'){map.removeLayer(lines[i]);}
		}
		route = [];
		coord = [];
		lines = [];
		choiceOfVeh = [];
	}
}


// HANDLE BUTTONS AT MAP
/**
startAndEndPointDraw: add start and endpoitn to markers-array and add it to map
exportData: exports data and direct user to ThankYou.html while clicking the "Fertig"-button
centerToStartAndEndpoint: after clicking the "OK"-button centers the map according to the start and endposition 
getLatLng: gets address-String and build a new markers-array element with latitude and longitude of this address
startOKButton, endOKButton & clockOKButton are only used for coping with asynchronous js bahaviour (f. same)
testID: confirm inputs with enter
(f)startAndEndPointDraw: enable routing and center map to start and end point
removeAllMarkers: removes all markers, routs and local storage
**/

function exportData() {
	var i,allDangerFilled=1;
	for(i=0;i<dangerMessage.length;i++) {
		if(dangerMessage[i] == 'undefined') {
			allDangerFilled=0;
			dangerPoints[i].options.icon.options.iconSize=[80,80];
			map.removeLayer(dangerPoints[i]);
			dangerPoints[i].addTo(map);
		} 
	}
	for(i=0;i<dangerMessage.length;i++) {
		if(JSON.stringify(dangerPoints[i].options.icon.options.iconSize)=="[80,80]" && dangerMessage[i] != 'undefined') {
			dangerPoints[i].options.icon.options.iconSize=[30,30];
		}
	}
	if(isStartPressed && isEndPressed && isClockPressed && allDangerFilled) {
		var geojson = getGeoJson();
		if(!wantToTestDistance){
			var zw = new Date();
        	var zw2=[zw.getHours(),zw.getMinutes()];
            var name = 'geojson'+zw2[0]+'-'+zw2[1]+'.txt';
			var link = document.getElementById('downloadlink');
			link.download=name;
			link.href = makeTextFile(geojson);
			link.zIndex = '-1';
			link.style.display = 'block';
			link.click();
		}
		clearStorage();
		window.location = 'ThankYou.html';
	}
	if(allDangerFilled==0) {
		document.getElementById('bubble2').className='bubble2';
		document.getElementById('message2').className='message2';
		say2('<p style="font-size:30px;">Du hast die hervorgehobenen Gefahrenstellen markiert, aber keinen Grund angegeben! Willst du trotzdem den Routenplaner beenden?</p>' +
			'<input type="image" id="yesDangerReport" src="../images/OK.png" onclick="fillDangerMessage()" style="position:relative;left:20%;width:80px;height:80px;">'+ 
			'<input type="image" id="noDangerReport" src="../images/NO.png" onclick="goOnWithDangerReport()" style="position:relative;right:20%;width:80px;height:80px;">');
		document.getElementById('bubble').style.display='none';
		document.getElementById('message').style.display='none';
		highlightText('message2');
	}
}

function getLatLng(address,i,f) {
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode({'address':address},function(results,status){
				if(status === google.maps.GeocoderStatus.OK) {
					var pos = results[0].geometry.location;	
					var myIconUrl;
					if(i==0) {
						myIconUrl='../images/circle_blue_start.png';
						isStartPressed=true;
						if(f=='f') {
							document.getElementById('fstart').style.display='none';
							document.getElementById('fstartOK').style.display='none';
							document.getElementById('firstInput').style.zIndex="-1";
							say2('<p style="font-size:1.8vmax;">Wo m�chtest du denn hin? Klicke wieder auf den gr�nen Haken und gib dann deine <u>Zieladdresse</u> ein!' +
								 ' <u>Tipp:</u> Wenn du deine Zieladdresse nicht kennst, gib einfach die Stadt ein. Du kannst sp�ter den Routenplaner auch beenden, '+
								 'wenn du nicht am Enpunkt angelangt bist!</p>'+
								 '<input type="image" src="../images/OK.png" onclick="firstInputText(1)" style="position:relative;width:6.89%;height:26.67%;">');
					}
					} else{
						myIconUrl='../images/circle_red_end.png';
						isEndPressed=true;
						if(f=='f') {
							document.getElementById('firstInput').style.zIndex="-1";
							say2('<p>Wann m�chtest du losgehen? Tipp: Um die Uhrzeit einzustellen, drehe die Uhrzeiger mit gedr�ckter Maustaste!</p>' +
								 '<input type="image" src="../images/OK.png" onclick="firstInputText(2)" style="position:relative;width:6.89%;height:26.67%;">');
							document.getElementById('fend').style.display='none';
							document.getElementById('fendOK').style.display='none';
						}
					}
					var personalizedIcon = L.icon({
						iconUrl: myIconUrl,
						iconSize: [80,80]
					});
						markers[i] = L.marker([pos.lat(),pos.lng()],{icon:personalizedIcon});
					if(i==0) {
						markers[0].bindPopup("Klicke in den blauen Kreis um den ersten Wegpunkt zu zeichnen!",{closeButton:false});
						markers[0]._popup.options.offset.y = -50;
						markers[0].on('click',function(e){
							if(isInputsInserted){
								markers[0].options.icon.options.iconUrl = '../images/circle_blue_start_clicked.png';
								addMarker(e);
								isFirstClicked=true;
								markers[0].closePopup();
								document.getElementById('firstInput').style.zIndex="-1";
								document.getElementById('transportation').style.zIndex="3";
								mapZoomEnabled(true);
							}
						});
						map.setView(markers[0]._latlng,18);
					}
					if(i==1) {
						markers[1].on('click',function(e){
							if(isFirstClicked) {
								markers[1].options.icon.options.iconUrl = '../images/circle_red_end_clicked.png';
								addMarker(e);
								addRoute(markers.length-4,1);
								changeIcon(markers.length-2);
							}
						});
						markers[0].openPopup();
					}
					markers[i].addTo(map);
					setMarkersSize();
				}
				else {
					myAlert('Eine Addresse gibt es nicht. Kannst du deine Eingaben noch einmal �berpr�fen?');
					console.log('Geocode failed because of ' + status);
					if(i==0 && !isStartPressed && f=='f') {
						firstInputText(0);
						document.getElementById('fend').style.display='none';
						document.getElementById('fendOK').style.display='none';
					}
					if(i==1 && !isEndPressed && f=='f' && getStoredValue('address1')[0]==address) {
						document.getElementById('dateInput').style.display='none';
						document.getElementById('timeInput').style.display='none';
						document.getElementById('fclockOK').style.display='none';
						firstInputText(1);
					}
				}
				markers[0].closePopup();
	});
}

function startOKButton() {
	var i,address = document.getElementById('start').value;
	localStorage.setItem('address1',address);
	if (isStartPressed) {
		for(i=0;i<markers.length;i++) {
			if (i==1) {continue;}
			map.removeLayer(markers[i]);
		}
		var zw = markers[1];
		markers=[];
		markers[1]=zw;
		deleteRoute(3);
	}
	if (address==='') {
		myAlert('Du hast das Startfeld leer gelassen');
	} else {
		isFirstClicked=false;
		markers[0] = getLatLng(address,0,'a');
		isStartPressed=true;
	}
}

function endOKButton() {
	var i,address = document.getElementById('end').value;
	localStorage.setItem('address2',address);
	if (isEndPressed) {map.removeLayer(markers[1]);}
	if (address==='' ||  getStoredValue('address1')[0]==address) {
		myAlert('Du hast das Zielfeld leer gelassen oder zwei mal die selbe Addresse eingegeben!');
	} else {
		markers[1] = getLatLng(address,1,'a');
		isEndPressed=true;
	}
}

function removeAllMarkers() {
	var i;
	for(i=0;i<markers.length;i++) {
		map.removeLayer(markers[i]);
	}
	markers=[];
	for(i=0;i<route.length;i++) {
		if(route[i]!='undefined'){
			if(typeof route[i].removeFrom!='undefined'){route[i].removeFrom(map);}
			else{map.removeLayer(route[i]);}
		}
		if(typeof lines[i] != 'undefined') {map.removeLayer(lines[i]);}
	}
	route=[];
	coord=[];
	lines=[];
	isStartPressed = false;
	isEndPressed = false;
	isClockPressed = false;
	isFirstClicked = false;
	isInputsInserted = false;
	clearStorage();
	document.getElementById('time').value = '';
	document.getElementById('date').value = '';
	document.getElementById('start').value = '';
	document.getElementById('end').value = '';
	document.getElementById('fstart').value = '';
	document.getElementById('fend').value = '';
	document.getElementById("famount").selectedIndex = "0";
	initialize();
}

function fstartOKButton() {
	var address = document.getElementById('fstart').value;
	localStorage.setItem('address1',address);
	if (isStartPressed) {
		map.removeLayer(markers[0]);
	}
	if (address==='') {
		myAlert('Du hast das Startfeld leer gelassen');
	} else {
		markers[0] = getLatLng(address,0,'f');
		if(sound){audio[1].stop();audio[2].play();}
	}
}

function fendOKButton() {
	var address = document.getElementById('fend').value;
	localStorage.setItem('address2',address);
	if (isEndPressed) {map.removeLayer(markers[1]);}
	if (address==='' || getStoredValue('address1')[0]==address) {
		myAlert('Du hast das Zielfeld leer gelassen oder zwei mal die selbe Addresse eingegeben!');
	} else {
		markers[1] = getLatLng(address,1,'f');
		isEndPressed=true;
		if(sound){audio[2].stop();audio[3].play();}
	}
}

function fclockOKButton() {
	var isnum=true;
	startTime = getTime().hour + ':' + getTime().min;
	startDate = getYear()+'-'+getMonth()+'-'+getDay();
	var sdzw= startDate.split('-');
	var d = new Date(sdzw[0],sdzw[1]-1,sdzw[2]);
	var weekday = ["Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag"];
	document.getElementById('famount').options[1].text = 'Nur am ' + sdzw[2] + '.' + sdzw[1] + '.' + sdzw[0];
	document.getElementById('famount').options[2].text = 'Immer am ' + weekday[d.getDay()] + ' au�er in den Ferien';
	document.getElementById('famount').options[3].text = 'Immer am ' + weekday[d.getDay()];
	
	var zw1='',zw2='',zw3=startDate.split('-');
	if(zw3[1]<10){zw1=0;}
	if(zw3[2]<10){zw2=0;}
	startDate = zw2+zw3[2] + '-' + zw1 + zw3[1] + '-' + zw3[0];
	document.getElementById('date').value = startDate;
	zw1='';zw2='';
	zw3=startTime.split(':');
	if(zw3[0]<10){zw1=0;}
	if(zw3[1]<10){zw2=0;}
	startTime = zw1+zw3[0]+':'+zw2+zw3[1];
	document.getElementById('time').value = startTime;
	
	
	if (startDate!='' && startTime!='' && startTime!='00:00') {
		isEndClicked=false;
		document.getElementById('dateInput').style.display='none';
		document.getElementById('timeInput').style.display='none';
		document.getElementById('fclockOK').style.display='none';
		if(!isClockPressed) {
			document.getElementById('firstInput').style.zIndex="-1";
			say2('<p>Wie oft gehst du diesen Weg? <u>Tipp:</u> Klicke auf das Feld!</p>' +
				 '<input type="image" src="../images/OK.png" onclick="firstInputText(3)" style="position:relative;width:6.89%;height:26.67%;">');
			if(sound){audio[3].stop();audio[4].play();}
		}
		else {
			firstInputText('default');
			say('Klicke auf ein Transportmittel und dann auf den n�chsten Wegpunkt! <u>Tipp:</u> Wenn du mit dem Auto, U/S-Bahn oder Bus f�hrst, '+
			    'reicht es den Anfangs- und Endpunkt anzuklicken!');
		}
	} else {
		myAlert('Du hast keine Uhrzeit eingestellt!');
		isnum=false;
	}
	if(isnum) {
		isClockPressed=true;
	}
	if(isClockPressed) {/*ändere Zeitpunkt in popup*/}
}

function fstartAndEndPointDraw() {
	showAllInputs();
	if (isStartPressed && isEndPressed  && isClockPressed) {
		map.setView(markers[0]._latlng,18);
		isInputsInserted=true;
	} else {
		//myAlert('Du musst zuerst beide Adressen und die Uhrzeit mit den Knöpfen rechts neben den Eingabe-Feldern bestätigen');
	}
	document.getElementById('firstInput').style.zIndex="-1";
	document.getElementById('transportation').style.zIndex="3";
	say('Geschafft. Klicke auf ein Transportmittel und dann auf den Startpunkt um deinen ersten Punkt zu setzen!');
	highlightText('message');
	if(sound){audio[5].stop();audio[6].play();}
	mapZoomEnabled(true);
}

function famountOK() {
	if(amount!=0) {
		document.getElementById('famount').style.display='none';
		document.getElementById('famountOK').style.display='none';
		document.getElementById('firstInput').style.zIndex="-1";
		say2('<p style="font-size:1.4vmax;text-align:left;"><u>Tipp 1:</u> Wenn du mit dem Bus, U/S-Bahn oder Auto f�hrst, reicht es, den Anfangs- und Endpunkt anzugeben!<p>' +
			 '<p style="font-size:1.4vmax;text-align:left;"><u>Tipp 2:</u> Achte auf die Sprechblase links unten. Darin stehen wichtige Tipps!<p>' +
			 '<p style="font-size:1.4vmax;text-align:left;"><u>Tipp 3:</u> Wenn du fertig mit deiner Route bist, klicke auf den gr�nen Haken in der linken unteren Ecke!</p>' +
			 '<input type="image" src="../images/OK.png" onclick="firstInputText(4)" style="position:relative;width:6.89%;height:26.67%;">');
	}
}

function get_amount(e) {
	amount = e.options[e.selectedIndex].value;
}

function testID(arg) {
	var id = arg.getAttribute('id');
	if(event.keyCode == 13) {
		switch (id) {
			case 'start':
				startOKButton();
				break;
			case 'end':
				endOKButton();
				break;
			case 'fstart':
				fstartOKButton();
				break;
			case 'fend':
				fendOKButton();
				break;
			case 'famount':
				famountOK();
				break;
			default:
				if(id.indexOf('dangerPoints') > -1) {
					getDangerMessage(arg);
				} else {console.log('something went wrong');}
		}
	}	
}

function changeTransportation(arg) {
	id = arg.getAttribute('id');
	var value = document.getElementById(id).value;
	var pos = value * 20 - 4 + '%';
	document.getElementById('rectangle').style.top = pos;
	vehicle = id;
	if(vehicle=='bike' && routers == 'mapzen') {vehicle='bicycle';}
	if(vehicle=='bus' || vehicle=='underground' || vehicle=='car') {
		say('Wenn du mit dem Auto, Zug, U-Bahn, S-Bahn oder Bus f�hrst musst du nur Anfangs- und Endpunkt eingeben');
		highlightText('message');
	}
}

function changePicToExpl(arg) {
	var value = arg.getAttribute('value');
	var id = arg.getAttribute('id');
	id=id.split('_')[0];
	document.getElementById(id).src = '../images/'+id+value+'.png';
}

function changeExplToPic(arg) {
	var value = arg.getAttribute('value');
	var id = arg.getAttribute('id');
	document.getElementById(id).src = '../images/'+id+'_icon.png';
}

function endOfRoutingAndDanger() {
	exportData();
	clearStorage();

}

function dateAndTimeInput() {
	document.getElementById('bubble').style.display = 'none';
	document.getElementById('message').style.display = 'none';
	document.getElementById('bubble2').className='bubble2';
	document.getElementById('message2').className='message2';
	say2('<p>Wann m�chtest du losgehen? Tipp: Um die Uhrzeit einzustellen, drehe die Uhrzeiger mit gedr�ckter Maustaste!</p>' +
		 '<input type="image" src="../images/OK.png" onclick="firstInputText(2)" style="position:relative;width:6.89%;height:26.67%;">');
	isEndClicked=true;
}


// LOCAL STORAGE
/**
Deal with storing,reading and deleting the local storage 
[problem: objects have to be converted to a string and after loading separated again]
**/

function storeValue() {
	if (markers.length>0) {
		var i=0,markersString='',choiceString='';
		for(i=0;i<markers.length-1;i++) {
			markersString += markers[i]._latlng + '|';
		}
		for(i=0;i<choiceOfVeh.length;i++) {
			choiceString += choiceOfVeh[i] + '|';
		}
		markersString += markers[markers.length-1]._latlng;
		if (localStorage) {
			localStorage.setItem('markers', markersString);
			localStorage.setItem('device', choiceOfVeh);
		}
	}
	/*if (dangerMarkers.length>0) {
		var i=0,markersString='';
		for(i=0;i<dangerMarkers.length-1;i++) {
			markersString += markers[i]._latlng + '|';
		}
		markersString += dangerMarkers[markers.length-1]._latlng;
		if (localStorage) {
			localStorage.setItem('dangerMarkers', markersString);
		}
	}
	if(dangerMessage.length>0) {
		var i,zw='';
		for(i=0;i<dangerMessage.length-1;i++){
			zw+=dangerMessage[i]+'|';
		}
		zw+=dangerMessage[dangerMessage.length-1];
	}*/
	if (startTime!=null || startTime!='') {
		localStorage.setItem('time',startTime);
	}
	if (startDate!=null || startDate!='') {
		localStorage.setItem('date',startDate);
	}
	if (amount!=0) {
		localStorage.setItem('amount',amount);
	}
}

function getStoredValue(key) {
	if(localStorage.getItem(key)!=null) {
		var str;
		if (localStorage) {
			str = localStorage.getItem(key);
		}
		return str.split("|");
	}
}

function getStoredMarkers(storedMarkers,str) {
	var i,zwVeh=[];
	if(str=='markers'){
	if(storedMarkers.length<4){isRouteInitialized=true;}
	for (i=0;i<storedMarkers.length;i++) {
		var zw1,zw2,zw3,zw4={};
		zw1=storedMarkers[i].split(")");
		zw2=zw1[0].split("(");
		zw3=zw2[1].split(",");
		zw4.lat=zw3[0];
		zw4.lng=zw3[1];
		var myIcon;
		if(i>1) {
			var e = new Object();
			e.latlng = new Object();
			e.latlng.lat = zw4.lat;
			e.latlng.lng = zw4.lng;
			if(i>2){
				choiceOfVeh[i-3] = storedDevice[i-3];
			}
			addMarker(e);
		}
		if(i==0) {
			if(storedMarkers.length>2){myIcon = L.icon({iconUrl:'../images/circle_blue_start_clicked.png' , iconSize :[80,80]});}
			else{myIcon = L.icon({iconUrl:'../images/circle_blue_start.png' , iconSize :[80,80]});}
			markers[0] = L.marker([zw4.lat,zw4.lng],{icon:myIcon});
			markers[0].bindPopup("Klicke in den blauen Kreis um den ersten Wegpunkt zu zeichnen!",{closeButton:false});
			markers[0]._popup.options.offset.y = -50;
			markers[0].on('click',function(e){
				markers[0].options.icon.options.iconUrl = '../images/circle_blue_start_clicked.png';
				markers[0].addTo(map);
				addMarker(e);
				isFirstClicked=true;
				markers[1].on('click',function(e){
					if(isFirstClicked) {
						addMarker(e);
						addRoute(markers.length-4,1);
						changeIcon(markers.length-2);
					}
				});
				markers[0].closePopup();
			});
			markers[0].addTo(map);
		}
		if(i==1) {
			myIcon = L.icon({iconUrl:'../images/circle_red_end.png' , iconSize :[80,80]}); 
			markers[1] = L.marker([zw4.lat,zw4.lng],{icon:myIcon});
			markers[1].addTo(map);
			markers[1].on('click',function(e){
				if(isFirstClicked) {
					markers[1].options.icon.options.iconUrl = '../images/circle_red_end_clicked.png';
					addMarker(e);
					addRoute(markers.length-4,1);
					changeIcon(markers.length-2);
				}
			});
		}
	}
	
	if(markers.length>3) {
		var counter = 0;
		isRouteInitialized = false;
		var timerID = setInterval(function(){
			var i=0;
			for(i=0;i<markers.length-3;i++){
				if(typeof route[i] == 'undefined' && typeof lines[i] == 'undefined') {
					if(!isRouteRunning){
						vehicle = choiceOfVeh[i];
						addRoute(i,1);
						if(counter == markers.length-4){
								for(i=2;i<markers.length;i++){
									if(i==2){vehicle=choiceOfVeh[i-2];}
									if(i==markers.length-1){vehicle=choiceOfVeh[i-3];}
									else {vehicle=choiceOfVeh[i-2];}
									changeIcon(i,true);
								}
							isRouteInitialized = true;
							clearInterval(timerID);
						}
						counter++;
					}
					break;
				}
			}
		},10);
	} else {
		isRouteInitialized = true;
	}
			
	if(!isFirstClicked) {
		markers[0].openPopup();	
	} else {
		markers[0].closePopup();
	}
	} /*else {
		for(i=0;i<storedMarkers.length;i++) {
			var zw1,zw2,zw3,zw4={};
			zw1=storedMarkers[i].split(")");
			zw2=zw1[0].split("(");
			zw3=zw2[1].split(",");
			zw4.lat=zw3[0];
			zw4.lng=zw3[1];
			var e = new Object();
			e.latlng = new Object();
			e.latlng.lat = zw4.lat;
			e.latlng.lng = zw4.lng;
			addDangerMarker(e);
		}
	}*/
}

function clearStorage() {
	localStorage.removeItem('markers');
	localStorage.removeItem('time');
	localStorage.removeItem('date');	
	localStorage.removeItem('address1');
	localStorage.removeItem('address2');
	localStorage.removeItem('device');
	localStorage.removeItem('amount');
	localStorage.removeItem('isEndClicked');
	localStorage.removeItem('dangerMarkers');
	localStorage.removeItem('dangerMessage');
}


// EXPORT DATA
/**
getGeoJson: get a geojson compatible string out of markers array (only coordinates), time Zone, times of destination at spliced route for exporting
getDistance: calculates distance between two latLng elements
download: download a .txt-file
prepareCoords: coordinates which appear twice in a row were deleted
getDistCoord: get distance array
split: get an array of coordinates which distance is less than 1
getTimeOfWaypoints: get time of destination at each splitted waypoint
**/

function getGeoJson() {
	var i;
	
	// prepare time and date
	var timeArray = startTime.split(':');
	var dateArray = startDate.split('-');
	var date = new Date(dateArray[0],dateArray[1]-1,dateArray[2],timeArray[0],timeArray[1]);
	var dateSplit = date.toString().split(' ');
	var weekday = ["sunday","monday","tuesday","wednesday","thursday","friday","saturday"];
	var weekly=false,daily=false,onlyAtSchoolday=false;
	switch (amount) {
		case '1':
			break;
		case '2':
			weekly=true;
			onlyAtSchoolday=true;
			break;
		case '3':
			weekly=true;
			break;
		case '4':
			weekly=true;
			daily=true;
			onlyAtSchoolday=true;
			break;
		default: console.log('Wrong frequency of route usage inserted');
	}
	
	
	// define geoJson object and fill properties
	var geoJsonData = new Object();
	geoJsonData.type = "FeatureCollection";
	geoJsonData.features = new Array();
	geoJsonData.properties = new Object();
	geoJsonData.properties.timeStamp = date;
	geoJsonData.properties.timeZone = dateSplit[5] + ' ' + dateSplit[6] + ' ' + dateSplit[7];
	geoJsonData.properties.frequency = new Object();
	geoJsonData.properties.frequency.day = weekday[date.getDay()];
	geoJsonData.properties.frequency.weekly = weekly;
	geoJsonData.properties.frequency.daily = daily;
	geoJsonData.properties.frequency.onlyAtSchoolday = onlyAtSchoolday;
	
	/* ================================================================================================================================ */
	if(wantToTestDistance) {
		var test=[];
	}
	/* ================================================================================================================================ */
	
	// MultiLineString for route
	prepareCoords();
	for(i=0; i<coord.length;i++) {
		var geoJsonMultiLineFeature = new Object();
		geoJsonMultiLineFeature.type = "Feature";
		geoJsonMultiLineFeature.geometry = new Object();
		geoJsonMultiLineFeature.geometry.type = "MultiLineString";	
		
		var j,routeArray =[],zwArray=[],distance;
		var divCoord=[];
		if(choiceOfVeh[i] == 'foot' || choiceOfVeh[i] == 'bike' || choiceOfVeh[i] == 'bicycle') {
			divCoord = split(coord[i].coordinates);
			distance="1";
			var routing;
			if(typeof route[i].removeFrom == 'undefined'){routing=false;}else{routing=true;}
			
			/* ================================================================================================================================ */
			if(wantToTestDistance) {
				var k;
				for(k=0;k<divCoord.length-1;k++){
					test.push(getDistance(divCoord[k][1],divCoord[k][0],divCoord[k+1][1],divCoord[k+1][0]));
				}
			}
			/* ================================================================================================================================ */
			
		} else {
			var zw = [coord[i].coordinates[0][1] , coord[i].coordinates[0][0]]; 
			divCoord.push(zw); 
			zw = [coord[i].coordinates[1][1] , coord[i].coordinates[1][0]]; 
			divCoord.push(zw);
			distance = getDistance(divCoord[0][1],divCoord[0][0],divCoord[1][1],divCoord[1][0]);
		}
		for(j=0;j<divCoord.length;j++) {
			zwArray[j]=[divCoord[j][0] , divCoord[j][1]];
		}
		routeArray.push(zwArray);
		geoJsonMultiLineFeature.geometry.coordinates = routeArray;
		
		geoJsonMultiLineFeature.properties = new Object();
		var devZw = choiceOfVeh[i];
		if(devZw=='bicycle'){devZw='bike';}
		geoJsonMultiLineFeature.properties.vehicle = devZw;
		geoJsonMultiLineFeature.properties.distance = distance;
		geoJsonMultiLineFeature.properties.routing = routing;
		geoJsonMultiLineFeature.properties.name = "Route";
		geoJsonData.features.push(geoJsonMultiLineFeature);
	}
	    
	/* ================================================================================================================================ */
	if(wantToTestDistance) {
		var max,min,k,av=0,plus=0,minus=0;
		max = Math.max.apply(Math, test); 
		min = Math.min.apply(Math, test);
		for(k=0;k<test.length;k++){
			av+=test[k];
			if(test[k]<=DIST*0.9) {minus++;}
			if(test[k]>DIST) {plus++;}
		}
		console.log('distances: ' , test);
		console.log('index of max: ' + test.indexOf(max) + '     |     max: ' + max);
		console.log('index of min: ' + test.indexOf(min) + '     |     min: ' + min);
		console.log('amount of < '+DIST*0.9+'m: ' + minus + '  |  amount of >'+DIST+'m: ' + plus);
		console.log('average distance: ' + av/test.length);
		console.log('percentage of errors: '+Math.round(100*100*(minus+plus)/test.length)/100+'%');
	}
	/* ================================================================================================================================ */
	
	// separat points for danger message
	for(i=0;i<dangerPoints.length;i++) {
		var geoJsonDangerReport = new Object();
		geoJsonDangerReport.type = "Feature";
		geoJsonDangerReport.geometry = new Object();
		geoJsonDangerReport.geometry.type = "Point"
		geoJsonDangerReport.geometry.coordinates = [dangerPoints[i]._latlng.lng , dangerPoints[i]._latlng.lat];
		geoJsonDangerReport.properties = new Object();
		geoJsonDangerReport.properties.reason = dangerMessage[i];
		geoJsonDangerReport.properties.name = "Danger Report"
		geoJsonData.features.push(geoJsonDangerReport);
	}

	// stringify geoJson object
	var geoJsonString = JSON.stringify(geoJsonData);
	return geoJsonString;
}

function getDistance(lat1, lng1, lat2, lng2){ 
    var R = 6371.000785; // Radius of earth (averaged) in KM (äquatorial: 6378.137 km)
    var dLat = (lat2 - lat1) * Math.PI / 180;
    var dLng = (lng2 - lng1) * Math.PI / 180;
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
			Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
			Math.sin(dLng/2) * Math.sin(dLng/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    return d * 1000; // meters
}

function makeTextFile (text) {
	var textFile = null;
	var data = new Blob([text],{type:'text/plain'});
	if(textFile !== null) {
		window.URL.revokeObjectURL(textFile);
	}
	textFile = window.URL.createObjectURL(data);
	return textFile;
}

function prepareCoords() {
	var i;
	for(i=0; i<coord.length || i<lines.length; i++) {
		if(typeof coord[i] == 'undefined') {
			coord[i] = new Object();
			coord[i].coordinates = new Array();
			coord[i].coordinates[0] = [lines[i]._latlngs[0].lat , lines[i]._latlngs[0].lng];
			coord[i].coordinates[1] = [lines[i]._latlngs[1].lat , lines[i]._latlngs[1].lng];
		} else {
			if(i!=coord.length) {
				var len = coord[i].coordinates.length-1;
				if(JSON.stringify(coord[i].coordinates[len]) != JSON.stringify([markers[i+3]._latlng.lat , markers[i+3]._latlng.lng])) {
					coord[i].coordinates.push([markers[i+3]._latlng.lat , markers[i+3]._latlng.lng]);
				}
			}
			if(i!=0) {
				if(JSON.stringify(coord[i].coordinates[0]) != JSON.stringify([markers[i+2]._latlng.lat , markers[i+2]._latlng.lng])) {
					var zw = [markers[i+2]._latlng.lat , markers[i+2]._latlng.lng];
					coord[i].coordinates = [zw].concat(coord[i].coordinates);
				}
			}
		}
	}
}

function getDistCoord(coord) {
	var i,dist=[];
	for(i=0;i<coord.length-1;i++) {
		dist[i] = getDistance(coord[i][0],coord[i][1],coord[i+1][0],coord[i+1][1]);
	}
	return dist;
}

function split(a) {
	var j,arr=[];
	var distance = getDistCoord(a);
	for(j=0;j<a.length-1;j++) {
		if(distance[j]>DIST) {
			var i,lam=1/(Math.ceil(distance[j])/DIST);
			for(i=0;i<Math.ceil(distance[j]/DIST);i++) {
				arr.push( [ (1-i*lam)*a[j][1] + i*lam*a[j+1][1], (1-i*lam)*a[j][0] + i*lam*a[j+1][0] ]   );
			}
		} 
		if(j<a.length-1 && distance[j]<=DIST && distance[j-1]<=DIST && j>0) {
			arr.push([a[j-1][1] , a[j-1][0]]);
		}
	}
	arr.push([a[a.length-1][1],a[a.length-1][0]]);
	return arr;
}


// DANGER REPORT
/**

**/

function routingEnded() {
	hideInputs();
	document.getElementById('bubble2').className='bubble2';
	document.getElementById('message2').className='message2';
	say2('<p style="font-size:1.8vmax;">Du hast es fast geschafft! Gibt es gef�hrliche Stellen auf deinem Weg?'+
		 '<u>Tipp:</u> Wenn du die Karte drucken m�chtest, klicke rechts oben auf den Zur�ck-Pfeil'+
		 ' und klicke auf den Drucker links oben!</p>' +
		 '<input type="image" id="yesDangerReport" src="../images/OK.png" onclick="firstInputText(5)" style="position:relative;left:20%;width:6.89%;height:26.67%;">'+ 
		 '<input type="image" id="noDangerReport" src="../images/NO.png" onclick="clearStorage() ,  exportData()" style="position:relative;right:20%;width:6.89%;height:26.67%;">');
	if(sound){audio[7].stop();audio[8].play();}
	document.getElementById('bubble').style.display='none';
	document.getElementById('message').style.display='none';
	highlightText('message2');
	document.getElementById('backToRouting').style.zIndex='3';
	document.getElementById('backToRoutingButton').style.display='block';
	document.getElementById('endOfRoutingAndDanger').style.display='none';
	isEndClicked=true;
	localStorage.setItem('isEndClicked', 1);
	storeValue();
	document.getElementById('dangerMod').style.zIndex='3';
}

function startDangerReport() {
	say('Wo gibt es denn eine gef�hrliche Stelle? Klicke auf diese Stelle.');
	highlightText('message');
	if(sound){audio[8].stop();audio[9].play();}
	document.getElementById('dangerMod').style.zIndex='3';
	showInput('endOfRoutingAndDanger');
	document.getElementById('backToRouting').style.zIndex='3';
	document.getElementById('bubble2').className='hidden';
	document.getElementById('message2').className='hidden';
	showInput('bubble');
	showInput('message');
	prepareMarkersForDangerReport();
	prepareLinesForDangerReport();
	showInput('zoom');
	showInput('printer');
	document.getElementById('zoom').style.top = '1%';
	document.getElementById('printer').style.top = '1%';
}

function prepareMarkersForDangerReport() {
	var i;
	for(i=0;i<markers.length;i++) {
		map.removeLayer(markers[i]);
		var iconZw;
		if(typeof markers[i].options.icon.options.iconUrl != 'undefined') {
			iconZw = L.icon({iconUrl:markers[i].options.icon.options.iconUrl , iconSize : markers[i].options.icon.options.iconSize});
			dangerMarkers[i] = L.marker(markers[i]._latlng , {'draggable':false, 'icon': iconZw });
		} else {
			dangerMarkers[i] = L.marker(markers[i]._latlng , {'draggable': false });
		}
		if(i>1) {
			dangerMarkers[i].on('click',function(e){
				var i,test=true;
				for(i=0;i<dangerPoints.length;i++) {
					if(JSON.stringify(dangerPoints[i]._latlng)==JSON.stringify(e.latlng)) {
						test=false;
					}
				}
				if(test) {
					addDangerMarker(e);
				}	
			});
		}
		dangerMarkers[i].addTo(map);
	}
}

function prepareLinesForDangerReport() {
	var i;
	for(i=0;i<markers.length-3;i++) {
		if(typeof lines[i] != 'undefined') {
			lines[i].options.clickable = true;
			lines[i].on('click',function(e){addDangerMarker(e);});
			// Eigentlich unn�tig, da dort keine Gefahrensituationen auftreten!
		} 
		if(typeof route[i] != 'undefined'){
			if(typeof route[i].removeFrom == 'undefined'){
				route[i].options.clickable = true;
				route[i].on('click',function(e){addDangerMarker(e);});
			}
		}
	}
}

function getDangerMessage(arg) {
	var id = arg.getAttribute('id');
	if(id.indexOf('OK')==-1){id='OK'+id;}
	var index = id.split('s')[1];
	dangerMessage[index] = document.getElementById(id.split('OK')[1]).value;
	if(dangerMessage[index]!='') {
		map.removeLayer(dangerPoints[index]);
		dangerPoints[index].closePopup();
		dangerPoints[index].bindPopup(
			'<input type="text" name=' +index+ ' id=' + index
			+ ' placeholder="Warum ist diese Stelle gef�hrlich?" value="' +dangerMessage[index]+ '" style="width:310px;height:40px;font-size:20px;" onkeyup="testID(this)"/>'
			+'<input type="button" id='+id+' value="OK" onclick="getDangerMessage(this)" style="width:155px;"/>'
			+'<input type="button" value="L�schen" class="marker-delete-button" style="width:155px;"/>'
		);
		dangerPoints[index]._popup.options.minWidth=330;
		dangerPoints[index].options.icon.options.iconSize=[30,30];
		dangerPoints[index].addTo(map);
		say('Super. Gibt es weitere Gefahrenstellen? Wenn nicht, dann klicke auf "Fertig" und du hast es geschafft.');
		if(sound){audio[10].stop();audio[11].play();}
	} 
}

function addDangerMarker(e) {
	var iconZw = L.icon({iconUrl:'../images/danger.png' , iconSize:[30,30]});
	var newMarker = L.marker(e.latlng,{icon:iconZw});
	var zw = "dangerPoints" + dangerPoints.length;
	var zw2 = '"OK' + zw + '"'; 
	zw = '"' + zw + '"';
	newMarker.bindPopup(
		'<input type="text" name='+zw+' id='+zw+' placeholder="Warum ist diese Stelle gef�hrlich?" style="width:310px;height:40px;font-size:20px;" onkeyup="testID(this)"/>'
		+'<input type="button" id='+zw2+' value="OK" onclick="getDangerMessage(this)" style="width:155px;"/>'
		+'<input type="button" value="L�schen" class="marker-delete-button" style="width:155px;"/>'
	);
	newMarker.on('popupopen', onPopupOpen);
	newMarker._popup.options.minWidth=330;
	dangerPoints.push(newMarker);
	dangerPoints[dangerPoints.length-1].addTo(map);
	dangerMessage.push('undefined');
	say('Du passt echt gut auf. Warum ist diese Stelle denn gef�hrlich? <u>Tipp:</u> Klicke auf die Stelle um einen Grund anzugeben.');
	highlightText('message');
	if(sound){audio[11].stop();audio[10].play();}
}

function backToRouting() {
	document.getElementById('backToRouting').style.zIndex='-1';
	document.getElementById('dangerMod').style.zIndex='-1';
	document.getElementById('bubble2').className='hidden';
	document.getElementById('message2').className='hidden';
	document.getElementById('message').style.zIndex='5';
	document.getElementById('bubble').style.zIndex='4';
	localStorage.removeItem('isEndClicked');
	isEndClicked=false;
	var i;
	for(i=0;i<dangerPoints.length;i++) {
		map.removeLayer(dangerPoints[i]);
	}
	if(typeof dangerMarkers[0] != 'undefined') {
		for(i=0;i<markers.length;i++){
			map.removeLayer(dangerMarkers[i]);
		}
	} else {
		for(i=0;i<markers.length;i++) {
			map.removeLayer(markers[i]);
		}
	}
	deleteRoute(3);
	document.getElementById('zoom').style.top = '15%';
	document.getElementById('printer').style.top = '15%';
	startLoading();
	var timerID = setInterval(function(){
		if(isRouteInitialized && isInitializeFinished){
			finishedLoading();
			clearInterval(timerID);
		}
	},10);
	initialize();
}

function downloadMap() {
	leafletImage(map, function(err, canvas) {
    // now you have canvas
    // example thing to do with that canvas:
    var img = document.createElement('img');
    var dimensions = map.getSize();
    img.width = dimensions.x;
    img.height = dimensions.y;
    img.src = canvas.toDataURL();
    document.getElementById('images').innerHTML = '';
    document.getElementById('images').appendChild(img);
	});
}

function goOnWithDangerReport() {
	document.getElementById('bubble2').className='hidden';
	document.getElementById('message2').className='hidden';
	document.getElementById('bubble').style.display='block';
	document.getElementById('message').style.display='block';
}

function fillDangerMessage() {
	var i;
	for(i=0;i<dangerMessage.length;i++){
		if(dangerMessage[i]='undefined'){
			dangerMessage[i]='No Reason';
		}
	}
	exportData();
}


// OTHERS
/**

**/

function openPopupByRightMouse(e) {
	if(e.which == 3) {
		var x = e.x;
		var y = e.y;
		var len = markers.length;
		var len2 = dangerPoints.length;
		var zwVeh2 = vehicle;
		vehicle = 'car';
		simulateClick(x,y);
		if(len != markers.length || len2!=dangerPoints.length){ 		
			var index = markers.length-1;
			if(!isEndClicked) {
				if(index>3) {
					var zwVeh = choiceOfVeh[index-4];
					if(zwVeh=='bicycle'){zwVeh='bike';}
					if(zwVeh=='bus'){zwVeh='H';}
					map.removeLayer(markers[index-1]);
					markers[index-1].options.icon = L.icon({iconUrl:'../images/'+zwVeh+'.png' , iconSize :[40,40]});
					markers[index-1].addTo(map);
				}
				deleteMarker(index);
				deleteRoute(index);
			} else {
				var index2 = dangerPoints.length-1;
				map.removeLayer(dangerPoints[index2]);
				dangerPoints.splice(index2,1);
				dangerMessage.splice(index2,1);
			}
		}
		vehicle = zwVeh2;
	}
}

function simulateClick(x, y) {
    var clickEvent= document.createEvent('MouseEvents');
    clickEvent.initMouseEvent(
		'click', true, true, window, 0,
		0, 0, x, y, false, false,
		false, false, 0, null
    );
    document.elementFromPoint(x, y).dispatchEvent(clickEvent);
}

function highlightText(identifier,speed,amount) {
	if(typeof speed == 'undefined'){speed=500;}
	if(typeof amount == 'undefined'){amount=10;}
	if(timerID!=-1){clearInterval(timerID);}
	var i=0,zwHTML = document.getElementById(identifier).innerHTML,zwHTML2;
	document.getElementById(identifier).innerHTML = '<b>'+zwHTML+'</b>';
	function zw() {
		
		if(i%2==0){
			document.getElementById(identifier).style.color='red';
		}else{
			document.getElementById(identifier).style.color='black';
		}
		i++;
		if(i==amount){
			document.getElementById(identifier).innerHTML = zwHTML;
			clearInterval(timerID);
		}
	}
	timerID=setInterval(zw,speed);
}

function changeSay(identifier,text,time) {
	if(typeof time == 'undefined'){time=5100;}
	if(timerID!=-1){clearInterval(timerID);}
	function zw() {
		say(text);
		highlightText(identifier);
		clearInterval(timerID);
	}
	timerID=setInterval(zw,time);
}

function printing() {
    javascript:void(0)($("#map").print({stylesheet:"../styles/easyPrint.css"}))
}

function openZoomer() {
	var bg = document.getElementById('zoomBackground').style;
	var plus = document.getElementById('plus').style;
	var minus = document.getElementById('minus').style;
	var zoomer = document.getElementById('zoomer').style;
	if(bg.height!='100%') {
		bg.height = '100%';
		plus.zIndex = '5';
		plus.display = 'block';
		minus.zIndex = '5';
		minus.display = 'block';
	} else {
		bg.height = '33%';
		plus.zIndex = '-1';
		plus.display = 'none';
		minus.zIndex = '-1';
		minus.display = 'none';
	}
	
}

function setToUserPosition() {
	if (window.navigator.geolocation) {
		var failure, success;
		success = function(position) {
			map.setView([position.coords.latitude,position.coords.longitude],18);
		};
		failure = function(message) {
			console.log('Cannot retrieve location!');
		};
		navigator.geolocation.getCurrentPosition(success, failure, {
			maximumAge: Infinity,
			timeout: 5000
		});
	}
}

function firstInputText(index) {
	document.getElementById('firstInput').style.zIndex="6";
	say2('');
	switch (index) {
		case 0:
			showInput('fstart');
			showInput('fstartOK');
			document.getElementById('fstart').focus();
			break;
		case 1:
			showInput('fend');
			showInput('fendOK');
			document.getElementById('fend').focus();
			break;
		case 2:
			showInput('dateInput');
			showInput('timeInput');
			showInput('fclockOK');
			break;
		case 3:
			showInput('famount');
			showInput('famountOK');
			break;
		case 4: 
			document.getElementById('firstInput').style.zIndex="-1";
			document.getElementById('bubble2').className='hidden';
			document.getElementById('message2').className='hidden';
			showInput('bubble');
			showInput('message');
			fstartAndEndPointDraw();
			markers[0].openPopup();
			break;
		case 5:
			document.getElementById('firstInput').style.zIndex="-1";
			say2('<p style="font-size:1.8vmax;text-align:left;"><u>Tipp 1:</u> Bevor du den Routenplaner beendest, klicke auf den Drucker links oben, damit du deine Karte drucken '+
			     'kannst!</p>'+
				 '<p style="font-size:1.8vmax;text-align:left;"><u>Tipp 2:</u> Wenn du alle Gefahrenpunkte markiert hast, klicke auf den gr�nen Haken links unten um den '+
				 'Routenplaner zu beenden!</p>'+
				 '<input type="image" src="../images/OK.png" onclick="startDangerReport()" style="position:relative;width:6.89%;height:26.67%;">');
			break;
		default:
			document.getElementById('firstInput').style.zIndex="-1";
			document.getElementById('bubble2').className='hidden';
			document.getElementById('message2').className='hidden';
			showInput('bubble');
			showInput('message');
	}
}

function myAlert(text) {
	document.getElementById('alertMessage').innerHTML = text;
	document.getElementById('myAlert').style.zIndex = '50';
}





/*
addEventListener('dblclick',function(e){storeToDropbox();console.log('dropbox upload');});
function storeToDropbox() {
	var options = {
    	files: [
			{'url': 'https://www.dropbox.com/static/images/koala_transparent.png', 'filename': 'geojson.png'}
    	],
    	success: function () {
        	alert("Success! Files saved to your Dropbox.");
    	},
    	progress: function (progress) {},
    	cancel: function () {},
    	error: function (errorMessage) {
			alert("upload failed!");
		}
	};
	if(Dropbox.isBrowserSupported()){Dropbox.save(options)};
}
*/
