'use strict';

/**
 * @ngdoc overview
 * @name csa2App
 * @description
 * # csa2App
 *
 * Main module of the application.
 */
angular
  .module('csa2App', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
	  .when('/tutorial',{
		  templateUrl: 'views/tutorial.html',
		  controller: 'MainCtrl'
	  })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
	  .when('/contact', {
		  templateUrl: 'views/contact.html',
		  controller: 'ContactCtrl'
	  })
      .otherwise({
        redirectTo: '/'
      });
  });
