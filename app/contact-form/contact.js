function submit() {
	var message = new Object();
	message.name = document.getElementById('name').value;
	message.email = document.getElementById('email').value;
	message.message = document.getElementById('message').value;
	
	if(message.message != '') {
		var zw = new Date();
		var zw2=[zw.getFullYear(),zw.getMonth()+1,zw.getDate(),zw.getHours(),zw.getMinutes()];
		var name = 'contact'+zw2[0]+'-'+zw2[1]+'-'+zw2[2]+'--'+zw2[3]+'-'+zw2[4]+'.txt';
		var link = document.getElementById('downloadlink');
		link.download=name;
		link.href = makeTextFile(JSON.stringify(message));
		link.zIndex = '-1';
		link.style.display = 'block';
		link.click();
		link.style.display = 'none';
		document.getElementById('name').value = '';
		document.getElementById('email').value = '';
		document.getElementById('message').value = '';
		document.getElementById('successMessage').innerHTML = 'Nachricht gesendet';
		document.getElementById('success').style.display = 'block';
		setTimeout(function(){document.getElementById('success').style.display = 'none';},3000);
	} else {
		document.getElementById('successMessage').innerHTML = 'Du hast keine Nachricht eingegeben!';
		document.getElementById('success').style.display = 'block';
		setTimeout(function(){document.getElementById('success').style.display = 'none';},3000);
	}
}

function makeTextFile (text) {
	var textFile = null;
	var data = new Blob([text],{type:'text/plain'});
	if(textFile !== null) {
		window.URL.revokeObjectURL(textFile);
	}
	textFile = window.URL.createObjectURL(data);
	return textFile;
}